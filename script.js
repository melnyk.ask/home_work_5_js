//1. Опишіть своїми словами, що таке метод об'єкту
// Метод об'єкту - це функція, яка щось виконує і може щось повертати. Метод може
// отримувати аргументи, може не отримувати. Метод може працювати з властивостями
// "свого" об'єкту.
//2. Який тип даних може мати значення властивості об'єкта?
// Властивості об'єкта можуть бути примітивами (number, string, boolean тощо),
// а можуть бути об'єктами.
//3. Об'єкт це посилальний тип даних. Що означає це поняття?
// Це означає, що змінна, в яку присвоєний об'єкт, буде зберігати не сам об'єкт,
// а посилання на нього (адресу в пам'яті). Таким чином один об'єкт можна присвоїти
// в декілька змінних. 



// 1. 
function createNewUser() {

    let firstName = prompt("Enter first name");
    let lastName = prompt("Enter last name");
    let newUser = {
        "firstName" : firstName,
        "lastName" : lastName,
        getLogin() { 
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();            
        },        
        setFirstName(str) { 
            Object.defineProperty(this, "firstName",
                {
                    value : str
                }
            );
        },
        setLastName(str) { 
            Object.defineProperty(this, "lastName",
                {
                    value : str
                }
            );
        }        
    };
    Object.defineProperty(newUser, "firstName",
        {
            writable: false,
            configurable: true
        }    
    );
    
    Object.defineProperty(newUser, "lastName",
        {
            writable: false,
            configurable: true
        }    
    );  
   
    return newUser;
}

let testUser = createNewUser();
console.log("1....", testUser);
console.log("2....", testUser.getLogin());


// 2.
let testUser2 = createNewUser();
console.log("3....", testUser2);

testUser2.firstName = "dsdsdsdsd";
testUser2.lastName = "dsdsdsdsd";
console.log("4.... Trying to change properties..",testUser2);

testUser2.setFirstName("set 1st name");
testUser2.setLastName("set last name");

console.log("5.... Using methods to change properties..", testUser2);
